/*
 * ASoC driver for Compulab CM-T335 module
 *
 * Copyright (C) 2013 CompuLab, Ltd.
 * Authors: Ilya Ledvich <ilya@compulab.co.il>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/soc.h>
#include <sound/soc-dai.h>
#include <sound/soc-dapm.h>

#define DRIVER_NAME		"cm-t335-soc-audio"

#define CODEC_CLOCK		12000000

static int cm_t335_hw_params(struct snd_pcm_substream *substream,
			     struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
	int ret;

	/* Set the codec system clock for DAC and ADC */
	ret = snd_soc_dai_set_sysclk(codec_dai, 0, CODEC_CLOCK,
				     SND_SOC_CLOCK_IN);
	if (ret < 0) {
		pr_err("CM-T335 ASoC: can't set codec system clock\n");
		return ret;
	}

	return 0;
}

static struct snd_soc_ops cm_t335_ops = {
	.hw_params = cm_t335_hw_params,
};

static const struct snd_soc_dapm_widget tlv320aic23_dapm_widgets[] = {
	SND_SOC_DAPM_HP("Headphone Jack", NULL),
	SND_SOC_DAPM_LINE("Line In", NULL),
	SND_SOC_DAPM_MIC("Mic Jack", NULL),
};

/* cm-t335 machine audio_mapnections to the codec pins */
static const struct snd_soc_dapm_route cm_t335_audio_map[] = {
	{"Headphone Jack", NULL, "LHPOUT"},
	{"Headphone Jack", NULL, "RHPOUT"},

	{"LLINEIN", NULL, "Line In"},
	{"RLINEIN", NULL, "Line In"},

	{"MICIN", NULL, "Mic Jack"},
};

static struct snd_soc_dai_link cm_t335_dai = {
	.name = "TLV320AIC23",
	.stream_name = "AIC23",
	.cpu_dai_name = "davinci-mcasp.1",
	.codec_dai_name = "tlv320aic23-hifi",
	.codec_name = "tlv320aic23-codec.1-001a",
	.platform_name = "davinci-pcm-audio",
	.dai_fmt = SND_SOC_DAIFMT_DSP_B | SND_SOC_DAIFMT_NB_NF |
			SND_SOC_DAIFMT_CBM_CFM,
	.ops = &cm_t335_ops,
};

static struct snd_soc_card cm_t335_snd_soc_card = {
	.name = "cm-t335",
	.dai_link = &cm_t335_dai,
	.num_links = 1,
	.dapm_widgets		= tlv320aic23_dapm_widgets,
	.num_dapm_widgets	= ARRAY_SIZE(tlv320aic23_dapm_widgets),
	.dapm_routes		= cm_t335_audio_map,
	.num_dapm_routes	= ARRAY_SIZE(cm_t335_audio_map),
};

static int __devinit cm_t335_soc_probe(struct platform_device *pdev)
{
	struct snd_soc_card *card = &cm_t335_snd_soc_card;
	int err;

	card->dev = &pdev->dev;

	err = snd_soc_register_card(card);
	if (err)
		dev_err(&pdev->dev, "sound card registration failed: %d\n",
			err);
	return err;
}

static int __devexit cm_t335_soc_remove(struct platform_device *pdev)
{
	struct snd_soc_card *card = platform_get_drvdata(pdev);

	snd_soc_unregister_card(card);
	card->dev = NULL;

	return 0;
}

static struct platform_driver cm_t335_driver = {
	.probe	= cm_t335_soc_probe,
	.remove	= __devexit_p(cm_t335_soc_remove),
	.driver	= {
		.name	= DRIVER_NAME,
		.owner	= THIS_MODULE,
	},
};

module_platform_driver(cm_t335_driver);

MODULE_AUTHOR("Ilya Ledvich <ilya@compulab.co.il>");
MODULE_DESCRIPTION("Compulab CM-T335 ASoC driver");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:" DRIVER_NAME);
