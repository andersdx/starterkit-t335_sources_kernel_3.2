/*
 * CompuLab CM-T335 module support
 *
 * Copyright (C) 2013 CompuLab, Ltd.
 * Authors: Ilya Ledvich <ilya@compulab.co.il>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/i2c/at24.h>
#include <linux/i2c/pca953x.h>
#include <linux/phy.h>
#include <linux/gpio.h>
#include <linux/spi/spi.h>
#include <linux/input.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/export.h>
#include <linux/wl12xx.h>
#include <linux/ethtool.h>
#include <linux/pwm_backlight.h>
#include <linux/input/ti_tsc.h>
#include <linux/platform_data/ti_adc.h>
#include <linux/mfd/ti_tscadc.h>
#include <linux/reboot.h>
#include <linux/pwm/pwm.h>
#include <linux/rtc/rtc-omap.h>
#include <linux/opp.h>

/* LCD controller is similar to DA850 */
#include <video/da8xx-fb.h>

#include <mach/hardware.h>
#include <mach/board-cm-t335.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/hardware/asp.h>

#include <plat/omap_device.h>
#include <plat/irqs.h>
#include <plat/board.h>
#include <plat/common.h>
#include <plat/lcdc.h>
#include <plat/usb.h>
#include <plat/emif.h>

#include "board-flash.h"
#include "cpuidle33xx.h"
#include "mux.h"
#include "devices.h"
#include "common.h"

/* Convert GPIO signal to GPIO pin number */
#define GPIO_TO_PIN(bank, gpio)			(32 * (bank) + (gpio))

/* Convert GPIO signal from the extender to pin pumber */
#define GPIO_EXT_TO_PIN(ebank, egpio)	(8 * (ebank) + (egpio) + 128)

typedef enum {
	CM_T335_DISP_DVI	= 0,
	CM_T335_DISP_LCD	= 1,
	CM_T335_DISP_LVDS	= 2,
	CM_T335_DISP_LCD0700	= 3,
	CM_T335_DISP_LCD0403	= 4,
	CM_T335_DISP_LCD0507	= 5,
} cm_t335_disp_type;

static cm_t335_disp_type cm_t335_disp;

static int __init parse_def_disp(char *arg)
{
	cm_t335_disp = CM_T335_DISP_DVI;

	if (!arg)
		return 0;

	if (!strcmp(arg, "lcd"))
		cm_t335_disp = CM_T335_DISP_LCD;
	else if (!strcmp(arg, "lcd0700"))
		cm_t335_disp = CM_T335_DISP_LCD0700;
	else if (!strcmp(arg, "lcd0403"))
		cm_t335_disp = CM_T335_DISP_LCD0403;
	else if (!strcmp(arg, "lcd0507"))
		cm_t335_disp = CM_T335_DISP_LCD0507;
	else if (!strcmp(arg, "lvds"))
		cm_t335_disp = CM_T335_DISP_LVDS;
	return 0;
}
early_param("cm_t335_disp", parse_def_disp);

#ifdef CONFIG_OMAP_MUX
static struct omap_board_mux cm_t335_board_mux[] __initdata = {
	/* GPIO Touchscreen Interrupt */
	AM33XX_MUX(GPMC_CSN2, OMAP_MUX_MODE7 | AM33XX_PIN_INPUT),	//Anders interrupt touchscreen line  --gpio1_31	
	/*Touch screen input*/
	AM33XX_MUX(AIN2, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT),
	AM33XX_MUX(AIN3, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT),
	AM33XX_MUX(AIN0, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT),
	AM33XX_MUX(AIN1, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT),

	/* GPIO LED */
	AM33XX_MUX(GPMC_CSN3, OMAP_MUX_MODE7 | AM33XX_PIN_INPUT),

	/* I2C 0 */
	AM33XX_MUX(I2C0_SDA, OMAP_MUX_MODE0 | AM33XX_SLEWCTRL_SLOW
				| AM33XX_PIN_INPUT),
	AM33XX_MUX(I2C0_SCL, OMAP_MUX_MODE0 | AM33XX_SLEWCTRL_SLOW
				| AM33XX_PIN_INPUT),
	/* I2C 1 */
	AM33XX_MUX(UART0_CTSN, OMAP_MUX_MODE3 | AM33XX_SLEWCTRL_SLOW
				| AM33XX_PIN_INPUT),
	AM33XX_MUX(UART0_RTSN, OMAP_MUX_MODE3 | AM33XX_SLEWCTRL_SLOW
				| AM33XX_PIN_INPUT),
	/* LCDC */
	AM33XX_MUX(LCD_DATA0, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA1, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA2, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA3, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA4, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA5, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA6, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA7, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA8, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA9, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA10, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA11, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA12, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA13, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA14, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(LCD_DATA15, OMAP_MUX_MODE0
				| AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA),
	AM33XX_MUX(GPMC_AD8, OMAP_MUX_MODE1 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(GPMC_AD9, OMAP_MUX_MODE1 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(GPMC_AD10, OMAP_MUX_MODE1 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(GPMC_AD11, OMAP_MUX_MODE1 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(GPMC_AD12, OMAP_MUX_MODE1 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(GPMC_AD13, OMAP_MUX_MODE1 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(GPMC_AD14, OMAP_MUX_MODE1 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(GPMC_AD15, OMAP_MUX_MODE1 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(LCD_VSYNC, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(LCD_HSYNC, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(LCD_PCLK, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(LCD_AC_BIAS_EN, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT),

	/* McASP */
	AM33XX_MUX(MII1_CRS, OMAP_MUX_MODE4 | AM33XX_PIN_INPUT_PULLDOWN),
	AM33XX_MUX(MII1_RXERR, OMAP_MUX_MODE4 | AM33XX_PIN_INPUT_PULLDOWN),
	AM33XX_MUX(MII1_COL, OMAP_MUX_MODE4 | AM33XX_PIN_INPUT_PULLDOWN),
	AM33XX_MUX(MII1_REFCLK, OMAP_MUX_MODE4 | AM33XX_PIN_INPUT_PULLDOWN),

	/* MMC 0 */
	AM33XX_MUX(MMC0_DAT3, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(MMC0_DAT2, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(MMC0_DAT1, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(MMC0_DAT0, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(MMC0_CLK, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(MMC0_CMD, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),

	/* NAND */
	AM33XX_MUX(GPMC_AD0, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(GPMC_AD1, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(GPMC_AD2, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(GPMC_AD3, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(GPMC_AD4, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(GPMC_AD5, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(GPMC_AD6, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(GPMC_AD7, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(GPMC_WAIT0, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(GPMC_WPN, OMAP_MUX_MODE7 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(GPMC_CSN0, OMAP_MUX_MODE0 | AM33XX_PULL_DISA),
	AM33XX_MUX(GPMC_ADVN_ALE, OMAP_MUX_MODE0 | AM33XX_PULL_DISA),
	AM33XX_MUX(GPMC_OEN_REN, OMAP_MUX_MODE0 | AM33XX_PULL_DISA),
	AM33XX_MUX(GPMC_WEN, OMAP_MUX_MODE0 | AM33XX_PULL_DISA),
	AM33XX_MUX(GPMC_BEN0_CLE, OMAP_MUX_MODE0 | AM33XX_PULL_DISA),

	/* Ethernet - RGMII 1 */
	AM33XX_MUX(MII1_TXEN, OMAP_MUX_MODE2 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(MII1_RXDV, OMAP_MUX_MODE2 | AM33XX_PIN_INPUT_PULLDOWN),
	AM33XX_MUX(MII1_TXD3, OMAP_MUX_MODE2 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(MII1_TXD2, OMAP_MUX_MODE2 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(MII1_TXD1, OMAP_MUX_MODE2 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(MII1_TXD0, OMAP_MUX_MODE2 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(MII1_TXCLK, OMAP_MUX_MODE2 | AM33XX_PIN_OUTPUT),
	AM33XX_MUX(MII1_RXCLK, OMAP_MUX_MODE2 | AM33XX_PIN_INPUT_PULLDOWN),
	AM33XX_MUX(MII1_RXD3, OMAP_MUX_MODE2 | AM33XX_PIN_INPUT_PULLDOWN),
	AM33XX_MUX(MII1_RXD2, OMAP_MUX_MODE2 | AM33XX_PIN_INPUT_PULLDOWN),
	AM33XX_MUX(MII1_RXD1, OMAP_MUX_MODE2 | AM33XX_PIN_INPUT_PULLDOWN),
	AM33XX_MUX(MII1_RXD0, OMAP_MUX_MODE2 | AM33XX_PIN_INPUT_PULLDOWN),
	AM33XX_MUX(MDIO_DATA, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(MDIO_CLK, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(EMU0, OMAP_MUX_MODE7 | AM33XX_PIN_INPUT_PULLUP),

	/* UART 0 */
	AM33XX_MUX(UART0_RXD, OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP),
	AM33XX_MUX(UART0_TXD, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT),

	{ .reg_offset = OMAP_MUX_TERMINATOR },
};
#else
#define	cm_t335_board_mux	NULL
#endif

/* module pin mux structure */
struct pinmux_config {
	const char *string_name; /* signal name format */
	int val; /* Options for the mux register value */
};

/* Module pin mux for eCAP0 */
static struct pinmux_config cm_t335_ecap0_pin_mux[] = {
	{"ecap0_in_pwm0_out.ecap0_in_pwm0_out",
		OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT},
	{NULL, 0},
};

/* Module pin mux for spi0 */
static struct pinmux_config cm_t335_spi0_pin_mux[] = {
	{"spi0_sclk.spi0_sclk", OMAP_MUX_MODE0 | AM33XX_PULL_ENBL
							| AM33XX_INPUT_EN},
	{"spi0_d0.spi0_d0", OMAP_MUX_MODE0 | AM33XX_PULL_ENBL | AM33XX_PULL_UP
							| AM33XX_INPUT_EN},
	{"spi0_d1.spi0_d1", OMAP_MUX_MODE0 | AM33XX_PULL_ENBL
							| AM33XX_INPUT_EN},
	{"spi0_cs0.spi0_cs0", OMAP_MUX_MODE0 | AM33XX_PULL_ENBL | AM33XX_PULL_UP
							| AM33XX_INPUT_EN},
	{"spi0_cs1.spi0_cs1", OMAP_MUX_MODE0 | AM33XX_PULL_ENBL | AM33XX_PULL_UP
							| AM33XX_INPUT_EN},
	{NULL, 0},
};

/* Module pin mux for uart1 */
static struct pinmux_config cm_t335_uart1_pin_mux[] = {
	{"uart1_ctsn.uart1_ctsn", OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT},
	{"uart1_rtsn.uart1_rtsn", OMAP_MUX_MODE0 | AM33XX_PIN_INPUT},
	{"uart1_rxd.uart1_rxd", OMAP_MUX_MODE0 | AM33XX_PIN_INPUT_PULLUP},
	{"uart1_txd.uart1_txd", OMAP_MUX_MODE0 | AM33XX_PULL_ENBL},
	{NULL, 0},
};

/* pinmux for wifi gpios */
static struct pinmux_config cm_t335_wlan_gpio_pin_mux[] = {
	{"xdma_event_intr1.gpio0_20", OMAP_MUX_MODE7 | AM33XX_PIN_OUTPUT},
	{"emu1.gpio3_8", OMAP_MUX_MODE7 | AM33XX_PIN_INPUT_PULLUP},
	{NULL, 0},
};

/* pinmux for bt gpio */
static struct pinmux_config cm_t335_bt_gpio_pin_mux[] = {
	{"xdma_event_intr0.gpio0_19", OMAP_MUX_MODE7 | AM33XX_PIN_OUTPUT},
	{NULL, 0},
};

/*
 * @pin_mux - single module pin-mux structure which defines pin-mux
 *			details for all its pins.
 */
static void setup_pin_mux(struct pinmux_config *pin_mux)
{
	int i;

	for (i = 0; pin_mux->string_name != NULL; pin_mux++)
		omap_mux_init_signal(pin_mux->string_name, pin_mux->val);

}

/* LCD backlight platform Data */
#define CM_T335_BACKLIGHT_MAX_BRIGHTNESS	100
#define CM_T335_BACKLIGHT_DEFAULT_BRIGHTNESS	100
#define CM_T335_PWM_PERIOD_NANO_SECONDS		(5000 * 10)

static struct platform_pwm_backlight_data cm_t335_backlight_pdata = {
	.pwm_id		= "ecap.0",
	.ch		= -1,
	.lth_brightness	= 21,
	.max_brightness	= CM_T335_BACKLIGHT_MAX_BRIGHTNESS,
	.dft_brightness	= CM_T335_BACKLIGHT_DEFAULT_BRIGHTNESS,
	.pwm_period_ns	= CM_T335_PWM_PERIOD_NANO_SECONDS,
};

/* Setup pwm-backlight */
static struct platform_device cm_t335_backlight = {
	.name           = "pwm-backlight",
	.id             = -1,
	.dev		= {
		.platform_data = &cm_t335_backlight_pdata,
	},
};

static struct pwmss_platform_data cm_t335_pwm_pdata = {
	.version = PWM_VERSION_1,
};

static int __init cm_t335_backlight_init(void)
{
	int err;

	setup_pin_mux(cm_t335_ecap0_pin_mux);

	err = am33xx_register_ecap(0, &cm_t335_pwm_pdata);
	if (err < 0) {
		pr_err("CM-T335: Failed to register ecap0: %d\n", err);
		return err;
	}

	err = platform_device_register(&cm_t335_backlight);
	if (err < 0)
		pr_err("CM-T335: Failed to register backlight device: %d\n",
		       err);

	return err;
}

static const struct display_panel cm_t335_disp_panel = {
	.panel_type	= WVGA,
	.max_bpp	= 32,
	.min_bpp	= 32,
	.panel_shade	= COLOR_ACTIVE,
};

static struct lcd_ctrl_config cm_t335_disp_cfg = {
	&cm_t335_disp_panel,
	.ac_bias		= 255,
	.ac_bias_intrpt		= 0,
	.dma_burst_sz		= 16,
	.bpp			= 32,
	.fdd			= 0x80,
	.tft_alt_mode		= 0,
	.stn_565_mode		= 0,
	.mono_8bit_mode		= 0,
	.invert_line_clock	= 1,
	.invert_frm_clock	= 1,
	.sync_edge		= 0,
	.sync_ctrl		= 1,
	.raster_order		= 0,
};

struct da8xx_lcdc_platform_data cm_t335_dvi_pdata = {
	.manu_name		= "DVI",
	.controller_data	= &cm_t335_disp_cfg,
	.type			= "DVI_1024x768@60",
};

struct da8xx_lcdc_platform_data cm_t335_lcd_pdata = {
	.manu_name		= "LCD",
	.controller_data	= &cm_t335_disp_cfg,
	.type			= "Startek_KD050C-WVGA",
};

struct da8xx_lcdc_platform_data cm_t335_lcd0700_pdata = {
	.manu_name		= "LCD0700",
	.controller_data	= &cm_t335_disp_cfg,
	.type			= "DataImage_SCF0700C48GGU33",
};

struct da8xx_lcdc_platform_data cm_t335_lcd0403_pdata = {
	.manu_name		= "LCD0403",
	.controller_data	= &cm_t335_disp_cfg,
	.type			= "DataImage_SCX0403C32GGC06",
};

struct da8xx_lcdc_platform_data cm_t335_lcd0507_pdata = {
	.manu_name		= "LCD0507",
	.controller_data	= &cm_t335_disp_cfg,
	.type			= "DataImage_FG050728DSSWDGT1",
};

struct da8xx_lcdc_platform_data cm_t335_lvds_pdata = {
	.manu_name		= "LVDS",
	.controller_data	= &cm_t335_disp_cfg,
	.type			= "NEC_NL10276BC20-12",
};

static int __init cm_t335_conf_disp_pll(int rate)
{
	struct clk *disp_pll;
	int ret = -EINVAL;

	disp_pll = clk_get(NULL, "dpll_disp_ck");
	if (IS_ERR(disp_pll)) {
		pr_err("Cannot clk_get disp_pll\n");
		goto out;
	}

	ret = clk_set_rate(disp_pll, rate);
	clk_put(disp_pll);
out:
	return ret;
}

#define CM_T335_DVI_EN_GPIO		GPIO_EXT_TO_PIN(1, 5)
#define CM_T335_LCD_EN_GPIO		GPIO_EXT_TO_PIN(1, 3)
#define CM_T335_LVDS_EN_GPIO		GPIO_EXT_TO_PIN(1, 1)

static int cm_t335_disp_init(unsigned gpio, const char *label, int rate,
			     struct da8xx_lcdc_platform_data *pdata)
{
	int err;

	/* Init display power enable GPIO */
	//err = gpio_request_one(gpio, GPIOF_OUT_INIT_LOW, label);
	err = gpio_request_one(gpio, GPIOF_OUT_INIT_HIGH, label);
	if (err < 0) {
		pr_err("CM-T335: %s gpio request failed: %d\n", label, err);
		return err;
	}

	gpio_export(gpio, 0);
	//gpio_set_value_cansleep(gpio, 1);
	gpio_set_value_cansleep(gpio, 0);

	err = cm_t335_conf_disp_pll(rate);
	if (err < 0) {
		pr_err("Failed to set pixclock to %d, not attempting to"
		       "register display adapter\n", rate);
		return err;
	}

	err = am33xx_register_lcdc(pdata);
	if (err < 0)
		pr_err("Failed to register CM-T335 %s panel\n",
		       pdata->manu_name);

	return err;
}

static void cm_t335_dvi_init(void)
{
	int err;

	/* Init SoC LCD controller */
	err = cm_t335_disp_init(CM_T335_DVI_EN_GPIO, "dvi_en",
				560000000, &cm_t335_dvi_pdata);
	if (err < 0)
		pr_err("CM-T335: DVI init failed: %d\n", err);
}

static void cm_t335_lcd_init(void)
{
	int err;

	/* Init SoC LCD controller */
	err = cm_t335_disp_init(CM_T335_LCD_EN_GPIO, "lcd_en",
				300000000, &cm_t335_lcd_pdata);
	if (err)
		pr_err("CM-T335: LCD init failed: %d\n", err);
}

static void cm_t335_lcd0700_init(void)
{
	int err;

	/* Init SoC LCD controller */
	err = cm_t335_disp_init(CM_T335_LCD_EN_GPIO, "lcd_en",
				300000000, &cm_t335_lcd0700_pdata);
	if (err)
		pr_err("CM-T335: LCD init failed: %d\n", err);
}

static void cm_t335_lcd0403_init(void)
{
	int err;

	/* Init SoC LCD controller */
	err = cm_t335_disp_init(CM_T335_LCD_EN_GPIO, "lcd_en",
				300000000, &cm_t335_lcd0403_pdata);
	if (err)
			pr_err("CM-T335: LCD init failed: %d\n", err);
}

static void cm_t335_lcd0507_init(void)
{
	int err;

	/* Init SoC LCD controller */
	err = cm_t335_disp_init(CM_T335_LCD_EN_GPIO, "lcd_en",
				300000000, &cm_t335_lcd0507_pdata);
	if (err)
			pr_err("CM-T335: LCD init failed: %d\n", err);
}

static void cm_t335_lvds_init(void)
{
	int err;

	/* Init SoC LCD controller */
	err = cm_t335_disp_init(CM_T335_LVDS_EN_GPIO, "lvds_en",
				300000000, &cm_t335_lvds_pdata);
	if (err)
		pr_err("CM-T335: LVDS init failed: %d\n", err);
}

#if defined(CONFIG_MFD_TI_TSCADC) || defined(CONFIG_MFD_TI_TSCADC_MODULE)
static struct tsc_data cm_t335_ts_data  = {
	.wires  = 4,
	.x_plate_resistance = 200,
	.steps_to_configure = 5,
};

static struct adc_data cm_t335_adc_data = {
	.adc_channels = 4,
};

static struct mfd_tscadc_board cm_t335_tscadc = {
	.tsc_init = &cm_t335_ts_data,
	.adc_init = &cm_t335_adc_data,
};

static void __init cm_t335_ts_init(void)
{
	int err;

	err = am33xx_register_mfd_tscadc(&cm_t335_tscadc);
	if (err)
		pr_err("CM-T335: failed to register touchscreen device\n");
}
#else
static inline void cm_t335_ts_init(void)
#endif

#if defined(CONFIG_MTD_NAND_OMAP2) || defined(CONFIG_MTD_NAND_OMAP2_MODULE)
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <plat/nand.h>

/* NAND partition information */
static struct mtd_partition cm_t335_nand_partitions[] = {
/* All the partition sizes are listed in terms of NAND block size */
	{
		.name		= "spl",
		.offset		= 0,			/* Offset = 0x0 */
		.size		= SZ_2M,
	},
	{
		.name		= "uboot",
		.offset		= MTDPART_OFS_APPEND,	/* Offset = 0x200000 */
		.size		= SZ_1M,
	},
	{
		.name		= "uboot environment",
		.offset		= MTDPART_OFS_APPEND,	/* Offset = 0x300000 */
		.size		= SZ_1M,
	},
	{
		.name		= "dtb",
		.offset		= MTDPART_OFS_APPEND,	/* Offset = 0x400000 */
		.size		= SZ_1M,
	},
	{
		.name		= "splash",
		.offset		= MTDPART_OFS_APPEND,	/* Offset = 0x500000 */
		.size		= SZ_4M,
	},
	{
		.name		= "linux",
		.offset		= MTDPART_OFS_APPEND,	/* Offset = 0x900000 */
		.size		= 6 * SZ_1M,
	},
	{
		.name		= "rootfs",
		.offset		= MTDPART_OFS_APPEND,	/* Offset = 0xF00000 */
		.size		= MTDPART_SIZ_FULL,
	},
};

static struct gpmc_timings cm_t335_nand_timings = {
	.sync_clk		= 0,

	.cs_on			= 0,
	.cs_rd_off		= 44,
	.cs_wr_off		= 44,

	.adv_on			= 6,
	.adv_rd_off		= 34,
	.adv_wr_off		= 44,
	.we_off			= 40,
	.oe_off			= 54,

	.access			= 64,
	.rd_cycle		= 82,
	.wr_cycle		= 82,

	.wr_access		= 40,
	.wr_data_mux_bus	= 0,
};

static void __init cm_t335_nand_init(void)
{
	struct omap_nand_platform_data *pdata;
	struct gpmc_devices_info gpmc_device[2] = {
		{ NULL, 0 },
		{ NULL, 0 },
	};

	pdata = omap_nand_init(cm_t335_nand_partitions,
		ARRAY_SIZE(cm_t335_nand_partitions), 0, 0,
		&cm_t335_nand_timings);
	if (!pdata)
		return;
	pdata->ecc_opt = OMAP_ECC_BCH8_CODE_HW;
	pdata->elm_used = true;
	gpmc_device[0].pdata = pdata;
	gpmc_device[0].flag = GPMC_DEVICE_NAND;

	omap_init_gpmc(gpmc_device, sizeof(gpmc_device));
	omap_init_elm();
}
#else
static inline void cm_t335_nand_init(void) {}
#endif

#if defined(CONFIG_MMC_OMAP_HS) || defined(CONFIG_MMC_OMAP_HS_MODULE)
#include <plat/mmc.h>
#include "hsmmc.h"

static struct omap2_hsmmc_info cm_t335_mmc[] __initdata = {
	{
		.mmc		= 1,
		.caps		= MMC_CAP_4_BIT_DATA,
		.gpio_cd	= -EINVAL,
		.gpio_wp	= -EINVAL,
		.ocr_mask	= MMC_VDD_32_33 | MMC_VDD_33_34, /* 3V3 */
	},
	{}	/* Terminator */
};

static void __init cm_t335_mmc0_init(void)
{
	omap2_hsmmc_init(cm_t335_mmc);
}
#else
static inline void cm_t335_mmc0_init(void) {}
#endif

#if defined(CONFIG_TI_CPSW) || defined(CONFIG_TI_CPSW_MODULE)

/* AM335X EVM Phy ID and Debug Registers */
#define AM335X_EVM_PHY_ID		0x4dd074
#define AM335X_EVM_PHY_MASK		0xfffffffe
#define AR8051_PHY_DEBUG_ADDR_REG	0x1d
#define AR8051_PHY_DEBUG_DATA_REG	0x1e
#define AR8051_DEBUG_RGMII_CLK_DLY_REG	0x5
#define AR8051_RGMII_TX_CLK_DLY		BIT(8)

static int cm_t335_tx_clk_dly_phy_fixup(struct phy_device *phydev)
{
	phy_write(phydev, AR8051_PHY_DEBUG_ADDR_REG,
		  AR8051_DEBUG_RGMII_CLK_DLY_REG);
	phy_write(phydev, AR8051_PHY_DEBUG_DATA_REG, AR8051_RGMII_TX_CLK_DLY);

	return 0;
}

static void __init cm_t335_eth_init(void)
{
	am33xx_cpsw_init(AM33XX_CPSW_MODE_RGMII, NULL, NULL);
	/* Atheros Tx Clk delay Phy fixup */
	phy_register_fixup_for_uid(AM335X_EVM_PHY_ID, AM335X_EVM_PHY_MASK,
				   cm_t335_tx_clk_dly_phy_fixup);
}
#else
static inline void cm_t335_eth_init(void)
#endif

#if defined(CONFIG_WL12XX_SPI) || defined(CONFIG_WL12XX_SPI_MODULE)

#define CM_T335_WLAN_EN_GPIO		GPIO_TO_PIN(0, 20)
#define CM_T335_WLAN_IRQ_GPIO		GPIO_TO_PIN(3, 8)

static struct gpio cm_t335_wlan_gpios[] = {
	{ CM_T335_WLAN_EN_GPIO, GPIOF_OUT_INIT_LOW, "wlan en" },
	{ CM_T335_WLAN_IRQ_GPIO, GPIOF_IN,  "wlan irq" },
};

static void cm_t335_wlan_set_power(bool enable)
{
	if (enable)
		gpio_set_value(CM_T335_WLAN_EN_GPIO, 1);
	else
		gpio_set_value(CM_T335_WLAN_EN_GPIO, 0);
}

struct wl12xx_platform_data cm_t335_wlan_pdata = {
	.set_power		= cm_t335_wlan_set_power,
	.board_ref_clock	= WL12XX_REFCLOCK_38_XTAL,
};

static struct spi_board_info cm_t335_wlan_spi_board_info = {
	.modalias		= "wl1271_spi",
	.bus_num		= 1,
	.chip_select		= 1,
	.irq			= OMAP_GPIO_IRQ(CM_T335_WLAN_IRQ_GPIO),
	.max_speed_hz		= 48000000,
	.platform_data		= &cm_t335_wlan_pdata,
};

static void __init cm_t335_wlan_init(void)
{
	int err;

	setup_pin_mux(cm_t335_wlan_gpio_pin_mux);

	err = gpio_request_array(cm_t335_wlan_gpios,
				 ARRAY_SIZE(cm_t335_wlan_gpios));
	if (err < 0) {
		pr_err("CM-T335: WLAN enable and irq gpio "
		       "request failed: %d\n", err);
		return;
	}
	gpio_export(CM_T335_WLAN_EN_GPIO, 0);

	err = spi_register_board_info(&cm_t335_wlan_spi_board_info, 1);
	if (err) {
		pr_err("CM-T335: register SPI board info failed: %d\n", err);
		goto gpio_free;
	}

	return;

gpio_free:
	gpio_free_array(cm_t335_wlan_gpios, ARRAY_SIZE(cm_t335_wlan_gpios));
}
#else
static inline void cm_t335_wlan_init(void) {}
#endif

#if defined(CONFIG_BT_HCIUART) || defined(CONFIG_BT_HCIUART_MODULE)

#define CM_T335_BT_EN_GPIO		GPIO_TO_PIN(0, 19)

static void __init cm_t335_bt_init(void)
{
	int err;

	setup_pin_mux(cm_t335_bt_gpio_pin_mux);
	setup_pin_mux(cm_t335_uart1_pin_mux);

	err = gpio_request_one(CM_T335_BT_EN_GPIO, GPIOF_OUT_INIT_LOW,
			       "bt en\n");
	if (err < 0) {
		pr_err("CM-T335: BT enable gpio request failed: %d\n", err);
		return;
	}

	gpio_export(CM_T335_BT_EN_GPIO, 0);
	udelay(100);

	gpio_set_value(CM_T335_BT_EN_GPIO, 1);
}
#else
static inline void cm_t335_bt_init(void) {}
#endif

static void __init cm_t335_wl12xx_init(void)
{
	cm_t335_wlan_init();
	cm_t335_bt_init();
}

static struct gpio_led cm_t335_gpio_leds[] = {
	{
		.name			= "cm_t335:green",
		.gpio			= GPIO_TO_PIN(2, 0),
		.active_low		= 1,
		.default_trigger	= "heartbeat",
	},
};

static struct gpio_led_platform_data cm_t335_gpio_led_info = {
	.leds		= cm_t335_gpio_leds,
	.num_leds	= ARRAY_SIZE(cm_t335_gpio_leds),
};

static struct platform_device cm_t335_leds_gpio = {
	.name	= "leds-gpio",
	.id	= -1,
	.dev	= {
		.platform_data	= &cm_t335_gpio_led_info,
	},
};

static void __init cm_t335_gpio_led_init(void)
{
	int err;

	err = platform_device_register(&cm_t335_leds_gpio);
	if (err)
		pr_err("failed to register gpio led device\n");
}

static struct omap_musb_board_data cm_t335_musb_board_data = {
	.interface_type	= MUSB_INTERFACE_ULPI,
	/*
	 * mode[0:3] = USB0PORT's mode
	 * mode[4:7] = USB1PORT's mode
	 * CM-T335 has USB0 in OTG mode.
	 */
	.mode		= MUSB_OTG,
	.power		= 500,
	.instances	= 0,
};

static struct cm_t335_eeprom_config cm_t335_eeprom;

static void cm_t335_eeprom_setup(struct memory_accessor *mem_acc, void *context)
{
	ssize_t ret;
	int size = sizeof(cm_t335_eeprom);

	ret = mem_acc->read(mem_acc, (char *)(&cm_t335_eeprom), 0, size);
	if (ret != size) {
		pr_warn("CM-T335: EEPROM read failed: %d\n", ret);
		return;
	}

	am33xx_cpsw_macidfillup((char *)(&cm_t335_eeprom.mac_addr), NULL);
	cm_t335_eth_init();
}

static void sb_t335_eeprom_setup(struct memory_accessor *mem_acc, void *context)
{
}

static int cm_t335_gpio_ext_setup(struct i2c_client *client, unsigned gpio,
				   unsigned ngpio, void *context)
{
	switch (cm_t335_disp) {
	case CM_T335_DISP_LCD:
		cm_t335_lcd_init();
		break;
	case CM_T335_DISP_LCD0700:
		cm_t335_lcd0700_init();
		break;
	case CM_T335_DISP_LCD0403:
		cm_t335_lcd0403_init();
		break;
	case CM_T335_DISP_LCD0507:
		cm_t335_lcd0507_init();
		break;
	case CM_T335_DISP_LVDS:
		cm_t335_lvds_init();
		break;
	case CM_T335_DISP_DVI:
	default:
		cm_t335_dvi_init();
		break;
	}

	return 0;
}

/* PCA9555 */
static struct pca953x_platform_data cm_t335_gpio_ext_pdata = {
	.gpio_base	= 128,
	.setup		= cm_t335_gpio_ext_setup,
};

static struct at24_platform_data cm_t335_eeprom_pdata = {
	.byte_len	= 256,
	.page_size	= 16,
	.setup		= cm_t335_eeprom_setup,
	.context	= (void *)NULL,
};

static struct at24_platform_data sb_t335_eeprom_pdata = {
	.byte_len	= 256,
	.page_size	= 16,
	.setup		= sb_t335_eeprom_setup,
	.context	= (void *)NULL,
};

static struct i2c_board_info cm_t335_i2c0_boardinfo[] __initdata = {
	{
		/* GPIO extender */
		I2C_BOARD_INFO("pca9555", 0x26),
		.platform_data = &cm_t335_gpio_ext_pdata,
	},
	{
		/* CM-T335 board EEPROM */
		I2C_BOARD_INFO("24c02", CM_T335_EEPROM_ADDR),
		.platform_data = &cm_t335_eeprom_pdata,
	},
	{
		/* SB-T335 board EEPROM */
		I2C_BOARD_INFO("24c02", SB_T335_EEPROM_ADDR),
		.platform_data = &sb_t335_eeprom_pdata,
	},
	{
		I2C_BOARD_INFO("em3027", 0x56),
	},
	{
		I2C_BOARD_INFO("tlv320aic23", 0x1a),
	},
};

static struct i2c_board_info cm_t335_i2c0_boardinfo_0700[] __initdata = {
	{
		/* GPIO extender */
		I2C_BOARD_INFO("pca9555", 0x26),
		.platform_data = &cm_t335_gpio_ext_pdata,
	},
	{
		/* CM-T335 board EEPROM */
		I2C_BOARD_INFO("24c02", CM_T335_EEPROM_ADDR),
		.platform_data = &cm_t335_eeprom_pdata,
	},
	{
		/* SB-T335 board EEPROM */
		I2C_BOARD_INFO("24c02", SB_T335_EEPROM_ADDR),
		.platform_data = &sb_t335_eeprom_pdata,
	},
	{
		I2C_BOARD_INFO("em3027", 0x56),
	},
	{
		I2C_BOARD_INFO("tlv320aic23", 0x1a),
	},
	{
		I2C_BOARD_INFO("scf0700_ts", 0x5C),
		.irq = OMAP_GPIO_IRQ(GPIO_TO_PIN(1,31)), //change Pin
	},
};

static struct i2c_board_info cm_t335_i2c0_boardinfo_0403[] __initdata = {
	{
		/* GPIO extender */
		I2C_BOARD_INFO("pca9555", 0x26),
		.platform_data = &cm_t335_gpio_ext_pdata,
	},
	{
		/* CM-T335 board EEPROM */
		I2C_BOARD_INFO("24c02", CM_T335_EEPROM_ADDR),
		.platform_data = &cm_t335_eeprom_pdata,
	},
	{
		/* SB-T335 board EEPROM */
		I2C_BOARD_INFO("24c02", SB_T335_EEPROM_ADDR),
		.platform_data = &sb_t335_eeprom_pdata,
	},
	{
		I2C_BOARD_INFO("em3027", 0x56),
	},
	{
		I2C_BOARD_INFO("tlv320aic23", 0x1a),
	},
	{
		I2C_BOARD_INFO("scx0403_ts", 0x5C),
		.irq = OMAP_GPIO_IRQ(GPIO_TO_PIN(1,31)), //change Pin
	},

};

static void __init cm_t335_i2c_init(void)
{
	int err=0;
	if (cm_t335_disp == CM_T335_DISP_LCD || cm_t335_disp == CM_T335_DISP_LCD0507) {
		omap_register_i2c_bus(1, 400, cm_t335_i2c0_boardinfo,
					ARRAY_SIZE(cm_t335_i2c0_boardinfo));
	}else if (cm_t335_disp == CM_T335_DISP_LCD0700) {
		omap_register_i2c_bus(1, 400, cm_t335_i2c0_boardinfo_0700,
					ARRAY_SIZE(cm_t335_i2c0_boardinfo_0700));
	}else if (cm_t335_disp == CM_T335_DISP_LCD0403) {
		omap_register_i2c_bus(1, 400, cm_t335_i2c0_boardinfo_0403,
					ARRAY_SIZE(cm_t335_i2c0_boardinfo_0700));
	}
	
	if ( cm_t335_disp == CM_T335_DISP_LCD0700 || cm_t335_disp == CM_T335_DISP_LCD0403) {
		err = gpio_request_one(GPIO_TO_PIN(1,31), GPIOF_IN, "TSPenDown"); // change pin
		printk("Define touchscreensection passed");
		if (err) {
			printk("Couldn't obtain gpio for TSPenDown: %d\n", err);
			pr_err("Couldn't obtain gpio for TSPenDown: %d\n", err);
			return;
		}
		gpio_export(GPIO_TO_PIN(1,31), 0); // change pin
	}
}

#if defined(CONFIG_SPI_OMAP24XX) || defined(CONFIG_SPI_OMAP24XX_MODULE)
#include <plat/mcspi.h>

static void __init cm_t335_spi_init(void)
{
	setup_pin_mux(cm_t335_spi0_pin_mux);

	omap_mcspi_pin_dir_set(1, MCSPI_PINDIR_D0_OUT_D1_IN);
}
#else
static inline void cm_t335_spi_init(void) {}
#endif

#if defined(CONFIG_SND_SOC_CM_T335) || \
	defined(CONFIG_SND_SOC_CM_T335_MODULE)
#include <plat/omap-pm.h>

static u8 cm_t335_iis_serializer_direction[] = {
	INACTIVE_MODE,	INACTIVE_MODE,	RX_MODE,	TX_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,	INACTIVE_MODE,
};

static struct snd_platform_data cm_t335_snd_data = {
	.tx_dma_offset	= 0x46400000,	/* McASP1 */
	.rx_dma_offset	= 0x46400000,
	.op_mode	= DAVINCI_MCASP_IIS_MODE,
	.num_serializer	= ARRAY_SIZE(cm_t335_iis_serializer_direction),
	.tdm_slots	= 2,
	.serial_dir	= cm_t335_iis_serializer_direction,
	.asp_chan_q	= EVENTQ_2,
	.version	= MCASP_VERSION_3,
	.txnumevt	= 32,
	.rxnumevt	= 32,
	.get_context_loss_count	=
			omap_pm_get_dev_context_loss_count,
};

static void __init cm_t335_mcasp_init(void)
{
	am335x_register_mcasp(&cm_t335_snd_data, 1);
}

static struct platform_device cm_t335_soc_audio = {
	.name	= "cm-t335-soc-audio",
	.id	= -1,
};

static void __init cm_t335_audio_init(void)
{
	cm_t335_mcasp_init();
	platform_device_register(&cm_t335_soc_audio);
}
#else
static inline void cm_t335_audio_init(void) {}
#endif

static void __init cm_t335_can_init(void)
{
	am33xx_d_can_init(0);
	am33xx_d_can_init(1);
}

void __iomem *am33xx_emif_base;

void __iomem * __init am33xx_get_mem_ctlr(void)
{

	am33xx_emif_base = ioremap(AM33XX_EMIF0_BASE, SZ_32K);

	if (!am33xx_emif_base)
		pr_warning("%s: Unable to map DDR2 controller",	__func__);

	return am33xx_emif_base;
}

void __iomem *am33xx_get_ram_base(void)
{
	return am33xx_emif_base;
}

void __iomem *am33xx_gpio0_base;

void __iomem *am33xx_get_gpio0_base(void)
{
	am33xx_gpio0_base = ioremap(AM33XX_GPIO0_BASE, SZ_4K);

	return am33xx_gpio0_base;
}

static struct resource cm_t335_cpuidle_resources[] = {
	{
		.start		= AM33XX_EMIF0_BASE,
		.end		= AM33XX_EMIF0_BASE + SZ_32K - 1,
		.flags		= IORESOURCE_MEM,
	},
};

/* AM33XX devices support DDR2 power down */
static struct am33xx_cpuidle_config cm_t335_cpuidle_pdata = {
	.ddr2_pdown	= 1,
};

static struct platform_device cm_t335_cpuidle_device = {
	.name			= "cpuidle-am33xx",
	.num_resources		= ARRAY_SIZE(cm_t335_cpuidle_resources),
	.resource		= cm_t335_cpuidle_resources,
	.dev = {
		.platform_data	= &cm_t335_cpuidle_pdata,
	},
};

static void __init cm_t335_cpuidle_init(void)
{
	int ret;

	cm_t335_cpuidle_pdata.emif_base = am33xx_get_mem_ctlr();

	ret = platform_device_register(&cm_t335_cpuidle_device);

	if (ret)
		pr_warning("CM_T335 cpuidle registration failed\n");

}

static void __init cm_t335_setup(void)
{
	cm_t335_gpio_led_init();
	cm_t335_i2c_init();
	cm_t335_spi_init();
	cm_t335_nand_init();
	cm_t335_mmc0_init();
	cm_t335_audio_init();
	cm_t335_ts_init();
	cm_t335_backlight_init();
	cm_t335_wl12xx_init();
	cm_t335_can_init();
}

static void __init cm_t335_init(void)
{
	cm_t335_cpuidle_init();
	am33xx_mux_init(cm_t335_board_mux);
	omap_serial_init();
	omap_sdrc_init(NULL, NULL);
	cm_t335_setup();
	usb_musb_init(&cm_t335_musb_board_data);
	/* Create an alias for icss clock */
	if (clk_add_alias("pruss", NULL, "pruss_uart_gclk", NULL))
		pr_warn("failed to create an alias: icss_uart_gclk --> pruss\n");
	/* Create an alias for gfx/sgx clock */
	if (clk_add_alias("sgx_ck", NULL, "gfx_fclk", NULL))
		pr_warn("failed to create an alias: gfx_fclk --> sgx_ck\n");
}

static void __init am335x_evm_map_io(void)
{
	omap2_set_globals_am33xx();
	omapam33xx_map_common_io();
}

MACHINE_START(CM_T335, "cm-t335")
	.atag_offset	= 0x100,
	.map_io		= am335x_evm_map_io,
	.init_early	= am33xx_init_early,
	.init_irq	= ti81xx_init_irq,
	.handle_irq     = omap3_intc_handle_irq,
	.timer		= &omap3_am33xx_timer,
	.init_machine	= cm_t335_init,
MACHINE_END

