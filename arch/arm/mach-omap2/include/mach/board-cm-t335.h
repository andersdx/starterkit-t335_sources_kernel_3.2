/*
 * Code for supporting AM335X EVM.
 *
 * Copyright (C) {2011} Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _BOARD_CM_T335_H
#define _BOARD_CM_T335_H

#define CM_T335_EEPROM_ADDR	0x50
#define SB_T335_EEPROM_ADDR	0x51


#ifndef __ASSEMBLER__

/* CM-T335 EEPROM layout */
#define EEPROM_MAC_ADDR_NUM	2
#define EEPROM_MAC_ADDR_LEN	6
#define EEPROM_NAME_LEN		16
#define EEPROM_PROD_OPT_NUM	3
#define EEPROM_PROD_OPT_LEN	16
#define EEPROM_MAJ_REV_LEN	2
#define EEPROM_MIN_REV_LEN	2
#define EEPROM_DATE_LEN		4
#define EEPROM_SERIAL_LEN	12

struct cm_t335_eeprom_config {
	char maj_rev[EEPROM_MAJ_REV_LEN];
	char min_rev[EEPROM_MIN_REV_LEN];
	char mac_addr[EEPROM_MAC_ADDR_NUM][EEPROM_MAC_ADDR_LEN];
	char prod_date[EEPROM_DATE_LEN];
	char serial_num[EEPROM_SERIAL_LEN];
	char wifi_mac_addr[EEPROM_MAC_ADDR_LEN];
	char bt_mac_addr[EEPROM_MAC_ADDR_LEN];
	char layout_ver;
	char res[83];
	char name[EEPROM_NAME_LEN];
	char prod_options[EEPROM_PROD_OPT_NUM][EEPROM_PROD_OPT_LEN];
};

extern void am33xx_cpsw_macidfillup(char *eeprommacid0, char *eeprommacid1);
extern void omap_mcspi_pin_dir_set(int spi_num, unsigned int direction);
extern void am33xx_d_can_init(unsigned int instance);
#endif
#endif /* _BOARD_CM_T335_H */
